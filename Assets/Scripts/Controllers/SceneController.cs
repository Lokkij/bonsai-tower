﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private bool loading = false;

    void Start()
    {
        InputReceiver.OnKeyPress += Reset;
    }

    void FixedUpdate()
    {
        if (PlayerController.Instance.transform.position.y < -10)
        {
            Reset(KeyCode.R);
        }
    }

    void Reset(KeyCode key)
    {
        if (key != KeyCode.R || loading) { return; }
        loading = true;
        InputReceiver.Reset();
        SceneManager.LoadScene("main");
    }
}
