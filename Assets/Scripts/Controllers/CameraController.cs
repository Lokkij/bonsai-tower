﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    void FixedUpdate()
    {
        transform.LookAt(new Vector3(0, 0, 0));
        if (PlayerController.Instance.transform.position.z == -5 || PlayerController.Instance.transform.position.z == 5)
        {
            transform.position = (new Vector3(PlayerController.Instance.transform.position.x * 2, transform.position.y, transform.position.z));
        }
        if (PlayerController.Instance.transform.position.x == -5 || PlayerController.Instance.transform.position.x == 5)
        {
            transform.position = (new Vector3(PlayerController.Instance.transform.position.x, transform.position.y, transform.position.z * 2));
        }
    }
}
