﻿using System.Collections;
using UnityEngine;
using UnityUtilities;

public class PlayerController : MonoBehaviour
{
    private const float Speed = 5;

    private Rigidbody PlayerRigidbody;
    private TowerLevel CurrentTowerLevel;

    public static PlayerController Instance { get; private set; }

    public bool GravityAffected
    {
        get { return PlayerRigidbody.useGravity; }
        set { PlayerRigidbody.useGravity = value; }
    }

    [HideInInspector]
    public bool CanClimbUp = false;
    [HideInInspector]
    public bool CanClimbDown = false;

    [HideInInspector]
    public bool LockMovement = true;

    public int AmountOfBonsai = 0;

    private void Awake()
    {
        Instance = this;
        PlayerRigidbody = gameObject.GetComponent<Rigidbody>();
        InputReceiver.Initialize();
        Cursor.visible = false;
    }

    #region Movement/rotation related
    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Cursor.visible = true;
        }

        if (LockMovement)
        {
            return;
        }

        float dx = transform.position.x + Input.GetAxis("Horizontal") * Time.deltaTime * Speed;
        float dy = transform.position.y;
        float dz = transform.position.z;

        var yDiff = Input.GetAxis("Vertical") * Time.deltaTime * Speed;
        if ((CanClimbUp && yDiff > 0) || (CanClimbDown && yDiff < 0))
        {
            dy += yDiff;
        }

        if (transform.position.x > Tower.Instance.RightEdgeX + 0.5f)
        {
            CoroutineProvider.Create(MoveXTo(transform.position.x + 0.65f));
            CoroutineProvider.Create(RotateTo(90));
            Tower.Instance.RotateTower(90);
            LockMovement = true;
        }
        else if (transform.position.x < Tower.Instance.LeftEdgeX - 0.5f)
        {
            CoroutineProvider.Create(MoveXTo(transform.position.x - 0.65f));
            CoroutineProvider.Create(RotateTo(-90));
            Tower.Instance.RotateTower(-90);
            LockMovement = true;
        }
        else
        {
            PlayerRigidbody.MovePosition(new Vector3(dx, dy, dz));
        }
    }

    /// <summary>
    /// WARNING: use only to fix the position when turning a corner.
    /// Unlocks movement when done.
    /// </summary>
    /// <param name="newPosition"></param>
    /// <returns></returns>
    private IEnumerator MoveXTo(float newX)
    {
        float timeSinceStarted = 0;
        Vector3 startingPosition = transform.position;
        Vector3 newPosition = new Vector3(newX, startingPosition.y, startingPosition.z);
        while (transform.position != newPosition)
        {
            timeSinceStarted += Time.deltaTime;
            transform.position = Vector3.Lerp(startingPosition, newPosition, timeSinceStarted / Tower.RotationTime);
            yield return null;
        }

        LockMovement = false;
    }

    private IEnumerator RotateTo(float yRotation)
    {
        var mesh = transform.FindChild("PlayerMesh");
        float timeSinceStarted = 0f;
        var goalRot = new Vector3(mesh.transform.rotation.x, mesh.rotation.y + yRotation, mesh.transform.rotation.z);
        while (timeSinceStarted < Tower.RotationTime)
        {
            timeSinceStarted += Time.deltaTime;
            mesh.transform.Rotate(Vector3.up, yRotation * (timeSinceStarted / Tower.RotationTime) * Time.deltaTime);
            yield return null;
        }
        mesh.transform.rotation = Quaternion.Euler(goalRot);
    }
    #endregion
}