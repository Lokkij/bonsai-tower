﻿using UnityEngine;

public class IntroCameraController : MonoBehaviour
{
    private const float Speed = 20f;

    void Start()
    {
        //InputReceiver.OnKeyPress += Kill;
    }

    void FixedUpdate()
    {
        transform.RotateAround(Vector3.zero, Vector3.up, -1f * Time.deltaTime * Speed);

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            GameObject.Find("Main Light").GetComponent<Light>().enabled = true;
            PlayerController.Instance.LockMovement = false;
            Destroy(gameObject);
            Destroy(this);
            InputReceiver.OnKeyPress -= Kill;
        }
    }

    void Kill(KeyCode key)
    {
        if (key != KeyCode.Space && key != KeyCode.Return)
        {
            return;
        }


    }
}
