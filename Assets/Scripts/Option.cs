using System;

/// <summary>
/// A wrapper to be used around any object that can be null.
/// Possile states are Some(value) and None.
/// </summary>
public class Option<T>
{
    private T Value;

    /// <summary>
    /// Returns whether this option contains a value.
    /// </summary>
    public bool HasValue { get { return Value != null; } }

    /// <summary>
    /// Returns whether this option contains a value.
    /// </summary>
    public bool IsSome { get { return HasValue; } }

    public Option(T value)
    {
        this.Value = value;
    }

    /// <summary>
    /// Returns the value of the option if it exists, and [otherwise] if it doesn't.
    /// </summary>
    public T ValueOr(T otherwise)
    {
        return IsSome ? this.Value : otherwise;
    }

    /// <summary>
    /// Returns the value of the option if it exists, and throws a NullReferenceException with [message] if it doesn't.
    /// </summary>
    public T Expect(string message = "Tried to get nonexistent value from Option.")
    {
        if (IsSome)
        {
            return Value;
        }
        else
        {
            throw new NullReferenceException(message);
        }
    }

    /// <summary>
    /// Runs the appropriate action on the option depending on whether or not the option has a value.
    /// </summary>
    /// <param name="None">
    /// The action to run if the option doesn't have a value.
    /// </param>
    /// <param name="Some">
    /// The action to run if the option does have a value.
    /// </param>
    public void Match(Action None, Action<T> Some)
    {
        if (IsSome)
        {
            Some(Value);
        }
        else
        {
            None();
        }
    }

    /// <summary>
    /// Runs the appropriate function on the option depending on whether or not the option has a value.
    /// </summary>
    /// <param name="None">
    /// The function to run if the option doesn't have a value.
    /// </param>
    /// <param name="Some">
    /// The function to run if the option does have a value.
    /// </param>
    public TOut Match<TOut>(Func<TOut> None, Func<T, TOut> Some)
    {
        if (IsSome)
        {
            return Some(Value);
        }
        else
        {
            return None();
        }
    }
}