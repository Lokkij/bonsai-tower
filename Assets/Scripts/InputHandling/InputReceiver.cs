﻿using System;
using System.Linq;
using UnityEngine;
using UnityUtilities;

public delegate void KeyPressHandler(KeyCode key);
public delegate void MouseClickHandler(MouseButton button, Option<GameObject> clickedObject);

public static class InputReceiver
{
    public static event KeyPressHandler OnKeyPress = delegate { };
    public static event MouseClickHandler OnMouseClick = delegate { };

    private static KeyCode[] KeysToListen;

    public static void Initialize()
    {
        KeysToListen = (Enum.GetValues(typeof(KeyCode)) as KeyCode[]).Where(x =>
            !x.ToString().ToUpper().Contains("JOYSTICK") && !x.ToString().ToUpper().Contains("MOUSE")
        ).ToArray();

        CoroutineProvider.OnUpdate += CheckForKeyPresses;
    }

    /// <summary>
    /// Checks whether a key has been pressed this frame, and if so lets the current UIContext handle it.
    /// </summary>
    static void CheckForKeyPresses()
    {
        if (!Input.anyKeyDown)
        {
            return;
        }

        foreach (KeyCode key in KeysToListen)
        {
            if (Input.GetKeyDown(key))
            {
                OnKeyPress(key);
            }
        }
    }

    public static void Reset()
    {
        OnKeyPress = delegate { };
    }
}
