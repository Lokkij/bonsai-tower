﻿/// <summary>
/// Contains all possible mouse buttons.
/// Can be cast from integers, assuming Unity's mouseubutton -> integer mapping.
/// </summary>
public enum MouseButton
{
    None = -1,
    Left = 0,
    Right = 1,
    Middle = 2
}