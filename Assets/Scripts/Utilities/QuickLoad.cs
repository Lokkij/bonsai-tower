﻿using System.Collections.Generic;
using UnityEngine;

namespace UnityUtilities
{
    static class QuickLoad
    {
        private static Dictionary<string, Object> CachedResources = new Dictionary<string, Object>();

        /// <summary>
        /// Loads the specified resource or returns a cached version if it has been loaded previously.
        /// </summary>
        /// <typeparam name="T">The type of resource you want to load.</typeparam>
        /// <param name="resourcePath">The path of the resource relative to the Assets/Resources folder.</param>
        public static T Load<T>(string resourcePath) where T : Object
        {
            if (CachedResources.ContainsKey(resourcePath))
            {
                return CachedResources[resourcePath] as T;
            }

            T resource = Resources.Load<T>(resourcePath);
            CachedResources.Add(resourcePath, resource);
            return resource;
        }
    }
}