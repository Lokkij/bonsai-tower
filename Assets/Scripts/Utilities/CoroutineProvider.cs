﻿using System;
using System.Collections;
using UnityEngine;

namespace UnityUtilities
{
    /// <summary>
    /// Provides coroutine functionality for non-MonoBehaviour classes through the Create function.
    /// Also provides an OnUpdate event for infinitely-running coroutines.
    /// Implemented as a singleton MonoBehaviour; needs to be attached to an empty in the first scene.
    /// </summary>
    public class CoroutineProvider : MonoBehaviour
    {
        /// <summary>
        /// Event that fires once every frame, in Unity's Update function.
        /// </summary>
        public static event Action OnUpdate = delegate { };

        private static CoroutineProvider Instance;

        private void Awake()
        {
            Instance = this;
        }

        private void Update()
        {
            OnUpdate();
        }

        /// <summary>
        /// Create and start [coroutine].
        /// For more information about coroutines, see the Unity manual: http://docs.unity3d.com/Manual/Coroutines.html
        /// </summary>
        /// <param name="coroutine">The coroutine to start.</param>
        public static void Create(IEnumerator coroutine)
        {
            Instance.StartCoroutine(coroutine);
        }
    }
}
