﻿using System;

namespace UnityUtilities
{
    public static class RNG
    {
        static Random rng = new System.Random();

        /// <summary>
        /// Returns the next random number in the range [min, max>.
        /// </summary>
        /// <param name="min">The inclusive minimum value.</param>
        /// <param name="max">The exclusive maximum value.</param>
        public static int Next(int min, int max)
        {
            return rng.Next(min, max);
        }
    }
}
