﻿using UnityEngine;

public class LadderSupportZone : MonoBehaviour
{
    void OnTriggerEnter(Collider coll)
    {
        PlayerController.Instance.GravityAffected = false;
        PlayerController.Instance.CanClimbDown = true;
    }

    void OnTriggerExit(Collider coll)
    {
        PlayerController.Instance.GravityAffected = true;
        PlayerController.Instance.CanClimbDown = false;
    }
}