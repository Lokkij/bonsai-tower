﻿using UnityEngine;

public class Ladder : MonoBehaviour
{
    void OnTriggerEnter(Collider coll)
    {
        PlayerController.Instance.CanClimbUp = true;
    }

    void OnTriggerExit(Collider coll)
    {
        PlayerController.Instance.CanClimbUp = false;
    }
}