﻿using System;
using System.Collections;
using UnityEngine;
using UnityUtilities;

public class PanelLever : MonoBehaviour
{
    public GameObject AttachedPanel;

    private Panel PanelScript;
    private BoxCollider Collider;

    public GameObject RevealsObject;

    private bool pulled = false;

    void Start()
    {
        this.PanelScript = AttachedPanel.GetComponent<Panel>();
        this.Collider = GetComponent<BoxCollider>();

        if (AttachedPanel == null || PanelScript == null)
        {
            throw new Exception("PanelLever missing attached Panel. Set it in the inspector.");
        }

        InputReceiver.OnKeyPress += (key) =>
        {
            if (key == KeyCode.E || key == KeyCode.Return) { Interact(); }
        };
    }

    void Interact()
    {
        if (!this.Collider.bounds.Intersects(PlayerController.Instance.GetComponent<BoxCollider>().bounds))
        {
            return;
        }

        if (pulled)
        {
            AudioSource.PlayClipAtPoint(QuickLoad.Load<AudioClip>("Sounds/LeverInvalid"), Camera.main.transform.position, 0.4f);
            return;
        }

        AudioSource.PlayClipAtPoint(QuickLoad.Load<AudioClip>("Sounds/LeverInteract"), Camera.main.transform.position);

        PanelScript.Disappear();

        if (RevealsObject != null)
        {
            StartCoroutine(FadeText());
        }

        pulled = !pulled;
        var oldRot = transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(oldRot.x, oldRot.y, oldRot.z + 180);
    }

    IEnumerator FadeText()
    {
        float timeSinceStarted = 0f;
        while (timeSinceStarted < 2f)
        {
            timeSinceStarted += Time.deltaTime;
            RevealsObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, timeSinceStarted / 2f);
            foreach(var child in RevealsObject.GetComponentsInChildren<SpriteRenderer>())
            {
                child.color = new Color(1, 1, 1, timeSinceStarted / 2f);
            }
            yield return null;
        }
    }
}
