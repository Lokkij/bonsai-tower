﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityUtilities;

public class Bonsai : MonoBehaviour
{
    public bool IsFinalBonsai = false;
    public bool IsBonusBonsai = false;
    bool bonusPickedUp = false;
    bool isLoading = false;

    void OnTriggerEnter()
    {
        if (!IsFinalBonsai)
        {
            AudioSource.PlayClipAtPoint(QuickLoad.Load<AudioClip>("Sounds/Bonsai"), Camera.main.transform.position);
            PlayerController.Instance.AmountOfBonsai++;
            if (IsBonusBonsai)
            {
                bonusPickedUp = true;
            }

            Destroy(gameObject);
            Destroy(this);
            return;
        }

        PlayerController.Instance.LockMovement = true;
        GameObject.Find("MessageVictory").GetComponent<SpriteRenderer>().enabled = true;
        GameObject.Find("MessageMusic2").GetComponent<SpriteRenderer>().enabled = true;
        if (!bonusPickedUp)
        {
            GameObject.Find("MessageVictoryBonus").GetComponent<SpriteRenderer>().enabled = true;
        }
        GameObject.Find("BackgroundMusic").GetComponent<AudioSource>().Stop();
        GameObject.Find("VictoryMusic").GetComponent<AudioSource>().Play();
        InputReceiver.OnKeyPress += VictoryDetection;

        Destroy(gameObject);
        Destroy(this);
    }

    void VictoryDetection(KeyCode key)
    {
        if (isLoading) { return; }
        isLoading = true;
        InputReceiver.Reset();
        SceneManager.LoadSceneAsync("main");
    }
}
