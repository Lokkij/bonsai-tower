﻿using System;
using System.Collections;
using UnityEngine;
using UnityUtilities;

public class TowerLevel : MonoBehaviour
{
    private const float RotationTime = 2.19f;

    public bool Rotating;

    private void Start()
    {
        Rotating = false;
        transform.FindChild("TowerLevelMesh").GetComponent<Renderer>().material.color *= 0.8f + Math.Abs(transform.position.y / 40f);
    }

    public void Rotate(RotationDirection direction, Action onComplete = null)
    {
        if (Rotating)
        {
            return;
        }

        Rotating = true;
        PlayerController.Instance.LockMovement = true;
        AudioSource.PlayClipAtPoint(QuickLoad.Load<AudioClip>("Sounds/TowerLevelRotation"), Camera.main.transform.position, 0.3f);
        CoroutineProvider.Create(RotateTo(transform.rotation.eulerAngles.y + (float)direction, new Option<Action>(onComplete)));
    }

    private IEnumerator RotateTo(float yRotation, Option<Action> onComplete)
    {
        float timeSinceStarted = 0f;
        var startRotation = transform.rotation;
        var goalRotation = Quaternion.Euler(startRotation.eulerAngles.x, yRotation, startRotation.eulerAngles.z);
        while (timeSinceStarted < RotationTime)
        {
            timeSinceStarted += Time.deltaTime;
            transform.rotation = Quaternion.Lerp(startRotation, goalRotation, timeSinceStarted / RotationTime);
            yield return null;
        }

        transform.rotation = goalRotation;

        Rotating = false;
        PlayerController.Instance.LockMovement = false;
        onComplete.Match(
            None: () => { },
            Some: (action) => action()
        );
    }
    
}

