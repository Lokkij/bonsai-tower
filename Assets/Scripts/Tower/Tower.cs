﻿using System.Collections;
using UnityEngine;
using UnityUtilities;

public class Tower : MonoBehaviour
{
    public const float RotationTime = 2f;

    public static Tower Instance { get; private set; }

    public float LeftEdgeX { get { return transform.position.x - 4.5f; } }
    public float RightEdgeX { get { return transform.position.x + 4.5f; } }

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// NOTE: this rotates the WHOLE tower, not just one single level.
    /// Rotates around the player and locks player movement during the rotation.
    /// </summary>
    public void RotateTower(float angle)
    {
        CoroutineProvider.Create(RotateTo(angle));
    }

    private IEnumerator RotateTo(float yRotation)
    {
        float timeSinceStarted = 0f;
        var goalRot = transform.rotation.eulerAngles.y + yRotation;
        while (timeSinceStarted < RotationTime)
        {
            timeSinceStarted += Time.deltaTime;
            transform.RotateAround(PlayerController.Instance.transform.position, Vector3.up, yRotation * (timeSinceStarted / RotationTime) * Time.deltaTime);
            yield return null;
        }
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, goalRot, transform.rotation.eulerAngles.z);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    }
}
