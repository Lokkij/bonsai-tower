﻿/// <summary>
/// Holds all possible rotation directions for TowerLevels.
/// Can be cast to integer/float to get the rotation modifier needed to perform the rotation.
/// </summary>
public enum RotationDirection
{
    None = 0,
    Right = -90,
    Left = 90,
    Double = 180
}

public static class RotationDirectionMethods
{
    /// <summary>
    /// WARNING: use only for directions to TowerLever.Rotate!
    /// </summary>
    public static RotationDirection Opposite(this RotationDirection rot)
    {
        if (rot == RotationDirection.Right)
        {
            return RotationDirection.Left;
        }
        else if (rot == RotationDirection.Left)
        {
            return RotationDirection.Right;
        }
        else
        {
            return RotationDirection.Double;
        }
    }
}