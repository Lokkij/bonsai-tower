﻿using System.Collections;
using UnityEngine;
using UnityUtilities;

public class Panel : MonoBehaviour
{
    private const float DisappearSpeed = 0.5f;
    private const float DisappearTime = 1.6f;

    void Appear()
    {
        float x = transform.position.x;
        float z = transform.position.z;

        if (transform.position.x == 4.2) x = 5;
        else if (transform.position.x == -4.2) x = -5;
        else if (transform.position.z == 4.2) z = 5;
        else if (transform.position.z == -4.2) z = -5;

        transform.position = Vector3.MoveTowards(transform.position, new Vector3(x, transform.position.y, z), 0.8f);
    }

    public void Disappear()
    {
        StartCoroutine(Disappear_Coroutine());
    }

    IEnumerator Disappear_Coroutine()
    {
        float timeSinceStarted = 0f;
        while (timeSinceStarted < DisappearTime)
        {
            timeSinceStarted += Time.deltaTime;
            transform.Translate(transform.forward * Time.deltaTime * DisappearSpeed);
            yield return null;
        }
    }
}
