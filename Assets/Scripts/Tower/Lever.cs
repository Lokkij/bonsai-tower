﻿using System;
using UnityEngine;
using UnityUtilities;

public class Lever : MonoBehaviour
{
    /// <summary>
    /// The TowerLevel this lever should rotate. Should be set in the inspector.
    /// </summary>
    public GameObject AttachedTowerLevel;

    /// <summary>
    /// The direction the attached TowerLevel should rotate in when this lever is activated.
    /// Should be set in the inspector.
    /// </summary>
    public RotationDirection RotationDirection = RotationDirection.None;

    public GameObject RevealsObject;

    private TowerLevel AttachedTowerLevelScript;
    private BoxCollider Collider;

    private bool pulled = false;

    void Start()
    {
        this.AttachedTowerLevelScript = AttachedTowerLevel.GetComponent<TowerLevel>();
        this.Collider = GetComponent<BoxCollider>();

        if (AttachedTowerLevel == null || AttachedTowerLevelScript == null)
        {
            throw new Exception("Lever missing attached TowerLevel. Set it in the inspector.");
        }

        InputReceiver.OnKeyPress += (key) =>
        {
            if (key == KeyCode.E || key == KeyCode.Return) { Interact(); }
        };
    }

    void Interact()
    {
        if (!this.Collider.bounds.Intersects(PlayerController.Instance.GetComponent<BoxCollider>().bounds))
        {
            return;
        }

        if (AttachedTowerLevelScript.Rotating)
        {
            AudioSource.PlayClipAtPoint(QuickLoad.Load<AudioClip>("Sounds/LeverInvalid"), Camera.main.transform.position, 0.4f);
            return;
        }

        AudioSource.PlayClipAtPoint(QuickLoad.Load<AudioClip>("Sounds/LeverInteract"), Camera.main.transform.position);

        var direction = pulled ? this.RotationDirection.Opposite() : this.RotationDirection;
        AttachedTowerLevelScript.Rotate(direction, onComplete: () => { });

        pulled = !pulled;

        if (RevealsObject != null)
        {
            var revealed = BonusBonsaiHandler.MessageSeen ? true : pulled;
            RevealsObject.GetComponent<Renderer>().enabled = revealed;
            var box = RevealsObject.GetComponent<BoxCollider>();
            if (box) { box.enabled = true; }

            foreach(var child in RevealsObject.GetComponentsInChildren<Renderer>())
            {
                if (child != null)
                {
                    child.enabled = revealed;
                }
            }
        }
        
        var oldRot = transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(oldRot.x, oldRot.y, oldRot.z + 180);
    }
}
